package com.cgi.assessment.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

@Component(service=Servlet.class,
property={
        Constants.SERVICE_DESCRIPTION + "=CGI Hello World Servlet",
        "sling.servlet.methods=" + HttpConstants.METHOD_POST,
        "sling.servlet.resourceTypes="+ "CGIAssessment/components/structure/page",
        "sling.servlet.extensions=" + "txt",
        "sling.servlet.paths=" + "/bin/hello"
})


public class CGIHelloWorldServlet extends SlingAllMethodsServlet {
    
    private static final long serialVersionUID = 1L;
    
    private static final String HELLO_WORLD = "Hello World from servlet!!!!";
    
    @Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) 
	        throws ServletException, IOException{
    	 response.setContentType("text/plain");
    	 response.getWriter().write(HELLO_WORLD);
    }
}


